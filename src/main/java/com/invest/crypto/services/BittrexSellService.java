package com.invest.crypto.services;

import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.annotation.Resource;
//import javax.naming.Context;
//import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;

public class BittrexSellService implements BittrexSellServiceMXBean {
	private static final Logger log = LogManager.getLogger(BittrexSellService.class);
	
	@Resource(name="InvestDS", lookup="java:jboss/datasources/InvestDS")
	private DataSource dataSource;
	
	private String testConfig;
	
	public String getTestConfig() {
		return testConfig;
	}
	
	public void setTestConfig(String testConfig) {
		this.testConfig = testConfig;
	}
    
    public void start() throws Exception {
    	log.info("BittrexSellService.start");
    	log.info("testConfig = " + testConfig);
    	
        try {
//            Context initCtx = new InitialContext();
//            DataSource dataSource = null;
            Connection conn = null;
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            try {
//                dataSource = (DataSource) initCtx.lookup("java:jboss/datasources/InvestDS");
                conn = dataSource.getConnection();
                pstmt = conn.prepareStatement("select  * from users where id = 1");
                rs = pstmt.executeQuery();
                if (rs.next()) {
                	log.info("Readed user: " + rs.getString("uname"));
                }
            } catch (Exception e) {
                log.error("", e);
            } finally {
            	if (null != rs) {
            		try {
            			rs.close();
            		} catch (Exception e) {
            			log.error("", e);
            		}
            	}
            	if (null != pstmt) {
            		try {
            			pstmt.close();
            		} catch (Exception e) {
            			log.error("", e);
            		}
            	}
            	if (null != conn) {
            		try {
            			conn.close();
            		} catch (Exception e) {
            			log.error("", e);
            		}
            	}
            }
        } catch (Exception e) {
            log.fatal("Cannot lookup datasource", e);
        }
    }

    public void stop() throws Exception {
    	log.info("BittrexSellService.stop");
    }
}