package com.invest.crypto.services.order.buy;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.bittrex.model.marketapi.OrderCreated;
import com.invest.crypto.bittrex.model.publicapi.Ticker;
import com.invest.crypto.services.order.OrderManager;

/**
 * @author Kiril.m
 */
class BuyOrderManager extends OrderManager {

	private static final Logger log = LogManager.getLogger(BuyOrderManager.class);

	static List<BuyOrder> getOrders(/* int stateId */) {
		Connection con = null;
		try {
			con = getConnection();
			return BuyOrderDAO.getOrders(con/* , stateId */);
		} catch (SQLException e) {
			log.debug("Unable to load orders in state 1", e);
			return new ArrayList<>();
		} finally {
			closeConnection(con);
		}
	}

	static boolean lockOrder(long orderId) {
		Connection con = null;
		try {
			con = getConnection();
			return BuyOrderDAO.lockOrder(con, orderId);
		} catch (SQLException e) {
			log.debug("Unable to lock order with id " + orderId, e);
			return false;
		} finally {
			closeConnection(con);
		}
	}

	static void unlockOrder(long orderId, String orderUuid) {
		Connection con = null;
		try {
			con = getConnection();
			if (!BuyOrderDAO.unlockOrder(con, orderId, orderUuid)) {
				log.warn("Unlock order update in the db did not execute as expected");
			}
		} catch (SQLException e) {
			log.debug("Unable to unlock order with id " + orderId, e);
		} finally {
			closeConnection(con);
		}
	}

	static OrderCreated placeBuyLimit(String apiKey, String market, BigDecimal amount) {
		try {
			Ticker ticker = getPublicApi().getTicker(market);
			BigDecimal quantity = amount.divide(ticker.getLast());
			BigDecimal rate = ticker.getLast().multiply(new BigDecimal(1.0025));
			// TODO apiKey should be used
			return getMarketApi().buyLimit(/* apiKey, */market, quantity, rate);
		} catch (IOException e) {
			log.debug("Unable to place buy limit order. Returning null", e);
			return null;
		} catch (InvalidKeyException | NoSuchAlgorithmException | IllegalStateException e) {
			log.debug("Unable to make buy limit request. Returning null", e);
			return null;
		}
	}
}