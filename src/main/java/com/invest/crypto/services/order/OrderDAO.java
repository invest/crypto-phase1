package com.invest.crypto.services.order;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.invest.crypto.services.order.OrderManager.OrderConfigKey;
import com.invest.crypto.util.DAOBase;

/**
 * @author Kiril.m
 */
public abstract class OrderDAO extends DAOBase {

	static OrderConfig getOrderConfig(Connection con) throws SQLException {
		String sql = "select key, value from crypto_config where key in(?, ?, ?)";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, OrderConfigKey.API_KEY.toString());
			ps.setString(2, OrderConfigKey.SCHEDULED_PERIOD.toString());
			ps.setString(3, OrderConfigKey.TASK_COUNT.toString());
			ResultSet rs = ps.executeQuery();
			String apiKey = null;
			long scheduledPeriod = 0;
			int taskCount = 0;
			while (rs.next()) {
				OrderConfigKey configKey = OrderConfigKey.valueOf(rs.getString("key"));
				switch (configKey) {
				case API_KEY:
					apiKey = rs.getString("value");
					break;

				case SCHEDULED_PERIOD:
					scheduledPeriod = rs.getLong("value");
					break;

				case TASK_COUNT:
					taskCount = rs.getInt("value");
					break;

				default:
					break;
				}
			}
			if (apiKey != null && scheduledPeriod > 0 && taskCount > 0) {
				return new OrderConfig(apiKey, scheduledPeriod, taskCount);
			}
			return null;
		}
	}
}