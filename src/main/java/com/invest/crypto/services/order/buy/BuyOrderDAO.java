package com.invest.crypto.services.order.buy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.invest.crypto.services.order.OrderDAO;

/**
 * @author Kiril.m
 */
class BuyOrderDAO extends OrderDAO {

	// TODO add fee
	static List<BuyOrder> getOrders(Connection con/*, int stateId*/) throws SQLException {
		String sql = "select id, market, amount, fee from crypto_trade where status = 1";
		List<BuyOrder> result = new ArrayList<>();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
//			ps.setInt(1, stateId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				BuyOrder order = new BuyOrder(rs.getLong("id"), rs.getString("market"), rs.getBigDecimal("amount"), rs.getBigDecimal("fee"));
				result.add(order);
			}
		}
		return result;
	}

	static boolean lockOrder(Connection con, long orderId) throws SQLException {
		String sql = "update crypto_trade set status = 2 where status = 1 and id = ?"; // update ... state = 2, rate = ? where state = 1
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, orderId);
			return ps.executeUpdate() == 1;
		}
	}

	static boolean unlockOrder(Connection con, long orderId, String orderUuid) throws SQLException {
		String sql = "update crypto_trade set status = 3, uuid = ?, time_opened = now() where id = ? and status = 2"; // update ... state = 3, uuid = ? where id = ? and state = 2
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, orderUuid);
			ps.setLong(2, orderId);
			return ps.executeUpdate() == 1;
		}
	}
}