package com.invest.crypto.services.order;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.bittrex.api.BittrexAccountAPI;
import com.invest.crypto.bittrex.api.BittrexMarketAPI;
import com.invest.crypto.bittrex.api.BittrexPublicAPI;
import com.invest.crypto.util.ManagerBase;

/**
 * @author Kiril.m
 */
public abstract class OrderManager extends ManagerBase {

	private static final Logger log = LogManager.getLogger(OrderManager.class);
	private static BittrexAccountAPI accountApi;
	private static BittrexMarketAPI marketApi;
	private static BittrexPublicAPI publicApi;

	public enum OrderConfigKey {
		API_KEY, SCHEDULED_PERIOD, TASK_COUNT;
	}

	public static OrderConfig getOrderConfig() {
		Connection con = null;
		try {
			con = getConnection();
			return OrderDAO.getOrderConfig(con);
		} catch (SQLException e) {
			log.debug("Unable to load config, Returning null", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}

	public static BittrexAccountAPI getAccountApi() {
		if (accountApi == null) {
			accountApi = new BittrexAccountAPI();
		}
		return accountApi;
	}

	public static BittrexMarketAPI getMarketApi() {
		if (marketApi == null) {
			marketApi = new BittrexMarketAPI();
		}
		return marketApi;
	}

	public static BittrexPublicAPI getPublicApi() {
		if (publicApi == null) {
			publicApi = new BittrexPublicAPI();
		}
		return publicApi;
	}
}