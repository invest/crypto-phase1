package com.invest.crypto.services.order.check;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.invest.crypto.services.order.OrderDAO;

/**
 * @author Kiril.m
 */
class CheckOrderDAO extends OrderDAO {

	static List<CheckOrder> getOrders(Connection con/* , int stateId */) throws SQLException {
		String sql = "select id, uuid, to_currency, quantity, wallet, transaction_id from crypto_trade where status = 3";
		List<CheckOrder> result = new ArrayList<>();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			// ps.setInt(1, stateId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				CheckOrder order = new CheckOrder(	rs.getLong("id"), rs.getString("uuid"), rs.getString("to_currency"),
													rs.getBigDecimal("quantity"), rs.getString("wallet"), rs.getLong("transaction_id"));
				result.add(order);
			}
		}
		return result;
	}

	static boolean closeOrder(Connection con, long orderId) throws SQLException {
		String sql = "update crypto_trade set status = 4, time_done = now() where id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, orderId);
			return ps.executeUpdate() == 1;
		}
	}

	static void insertWithdraw(Connection con, CheckOrder checkOrder) throws SQLException {
		String sql = "insert into crypto_withdraw (currency, quantity, wallet, transaction_id, crypt_trade_id, status, uuid) "
					+ "values (?, ?, ?, ?, ?, ?, ?)";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			int i = 1;
			ps.setString(i++, checkOrder.getToCurrency());
			ps.setBigDecimal(i++, checkOrder.getQuantity());
			ps.setString(i++, checkOrder.getWallet());
			ps.setLong(i++, checkOrder.getTransactionId());
			ps.setLong(i++, checkOrder.getId());
			ps.setInt(i++, 1); // TODO enum
			ps.setString(i++, checkOrder.getUuid());			
			ps.executeUpdate(); // TODO result?
		}
	}
}