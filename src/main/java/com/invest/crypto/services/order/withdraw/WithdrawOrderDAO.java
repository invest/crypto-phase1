package com.invest.crypto.services.order.withdraw;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.invest.crypto.services.order.OrderDAO;

/**
 * @author Kiril.m
 */
class WithdrawOrderDAO extends OrderDAO {

	static List<WithdrawOrder> getOrders(Connection con/* , int stateId */) throws SQLException {
		String sql = "select id, currency, quantity, wallet, from crypto_withdraw where status = 1";
		List<WithdrawOrder> result = new ArrayList<>();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			// ps.setInt(1, stateId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				WithdrawOrder order = new WithdrawOrder(rs.getLong("id"), rs.getString("currency"), rs.getBigDecimal("quantity"),
														rs.getString("wallet"));
				result.add(order);
			}
		}
		return result;
	}

	static boolean lockOrder(Connection con, long orderId) throws SQLException {
		String sql = "update crypto_withdraw set status = 2 where status = 1 and id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, orderId);
			return ps.executeUpdate() == 1;
		}
	}

	static boolean unlockOrder(Connection con, long orderId, String orderUuid) throws SQLException {
		String sql = "update crypto_trade set status = 3, uuid = ? where status = 2 and id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, orderUuid);
			ps.setLong(2, orderId);
			return ps.executeUpdate() == 1;
		}
	}
}