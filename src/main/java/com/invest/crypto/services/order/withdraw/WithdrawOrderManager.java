package com.invest.crypto.services.order.withdraw;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.bittrex.model.accountapi.WithdrawalRequested;
import com.invest.crypto.services.order.OrderManager;

/**
 * @author Kiril.m
 */
class WithdrawOrderManager extends OrderManager {

	private static final Logger log = LogManager.getLogger(WithdrawOrderManager.class);

	static List<WithdrawOrder> getOrders(/*int stateId*/) {
		Connection con = null;
		try {
			con = getConnection();
			return WithdrawOrderDAO.getOrders(con/*, stateId*/);
		} catch (SQLException e) {
			log.debug("Unable to load orders in state 1", e);
			return new ArrayList<>();
		} finally {
			closeConnection(con);
		}
	}

	static boolean lockOrder(long orderId) {
		Connection con = null;
		try {
			con = getConnection();
			return WithdrawOrderDAO.lockOrder(con, orderId);
		} catch (SQLException e) {
			log.debug("Unable to lock order with id " + orderId, e);
			return false;
		} finally {
			closeConnection(con);
		}
	}

	static void unlockOrder(long orderId, String orderUuid) {
		Connection con = null;
		try {
			con = getConnection();
			if (!WithdrawOrderDAO.unlockOrder(con, orderId, orderUuid)) {
				log.warn("Unlock order update in the db did not execute as expected");
			}
		} catch (SQLException e) {
			log.debug("Unable to unlock order with id " + orderId, e);
		} finally {
			closeConnection(con);
		}
	}

	static WithdrawalRequested placeWithdraw(String apiKey, String currency, BigDecimal quantity, String wallet) {
		try {
			// TODO apiKey should be used
			return getAccountApi().withdraw(/*apiKey, */currency, quantity, wallet);
		} catch (IOException e) {
			log.debug("Unable to place withdraw order. Returning null", e);
			return null;
		} catch (InvalidKeyException | NoSuchAlgorithmException | IllegalStateException e) {
			log.debug("Unable to make withdraw request. Returning null", e);
			return null;
		}
	}
}