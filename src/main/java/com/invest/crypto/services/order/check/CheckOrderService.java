package com.invest.crypto.services.order.check;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.services.order.OrderConfig;
import com.invest.crypto.services.order.OrderService;

/**
 * @author Kiril.m
 */
public class CheckOrderService extends OrderService {

	private static final Logger log = LogManager.getLogger(CheckOrderService.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.invest.crypto.services.order.OrderService#initService()
	 */
	@Override
	public void initService() {
		OrderConfig config = CheckOrderManager.getOrderConfig();
		if (config != null) {
			executor = new CheckOrderExecutor(config.getTaskCount());
			scheduledPeriod = config.getScheduledPeriod();
		} else {
			log.warn("Executor is null. Scheduling of task will fail");
		}
	}

	@Override
	protected Logger getLogger() {
		return log;
	}

	private class CheckOrderExecutor implements Runnable {

		private ExecutorService taskExecutor;

		public CheckOrderExecutor(int taskCount) {
			taskExecutor = Executors.newFixedThreadPool(taskCount);
		}

		@Override
		public void run() {
			log.debug("CheckOrderExecutor has started");
			List<CheckOrder> checkOrders = CheckOrderManager.getOrders();
			List<Future<?>> futures = new ArrayList<>();
			for (CheckOrder co : checkOrders) {
				futures.add(taskExecutor.submit(new CheckOrderTask(co)));
			}
			for (Future<?> future : futures) {
				try {
					future.get(); // in order to wait all tasks
				} catch (InterruptedException e) {
					log.debug("Thread was interrupted", e);
					// Preserve interrupt status
					Thread.currentThread().interrupt();
				} catch (ExecutionException e) {
					log.debug("Unable to retrieve result", e);
				}
			}
			log.debug("CheckOrderExecutor has completed");
		}
	}
}