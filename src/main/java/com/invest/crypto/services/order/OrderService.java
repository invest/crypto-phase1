package com.invest.crypto.services.order;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.Logger;

/**
 * @author Kiril.m
 */
public abstract class OrderService {

	protected ScheduledExecutorService scheduler;
	protected Runnable executor;
	protected long scheduledPeriod;

	public abstract void initService();

	protected abstract Logger getLogger();

	public void start() {
		getLogger().info(this.getClass() + ".start()");
		initService();
		scheduler = Executors.newScheduledThreadPool(1);
		scheduler.scheduleAtFixedRate(executor, 1, scheduledPeriod, TimeUnit.SECONDS);
	}

	public void stop() {
		getLogger().info(this.getClass() + ".stop()");
		scheduler.shutdown();
		try {
			// Wait a while for existing tasks to terminate
			if (!scheduler.awaitTermination(60, TimeUnit.SECONDS)) {
				scheduler.shutdownNow(); // Cancel currently executing tasks
				// Wait a while for tasks to respond to being cancelled
				if (!scheduler.awaitTermination(60, TimeUnit.SECONDS)) {
					getLogger().error("Unable to terminate scheduler");
				}
			}
		} catch (InterruptedException e) {
			getLogger().error("Unable to await termination", e);
			// (Re-)Cancel if current thread also interrupted
			scheduler.shutdownNow();
			// Preserve interrupt status
			Thread.currentThread().interrupt();
		} finally {
			if (!scheduler.isTerminated()) {
				scheduler.shutdownNow();
			}
		}
	}
}