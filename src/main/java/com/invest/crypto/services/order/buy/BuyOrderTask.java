package com.invest.crypto.services.order.buy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.bittrex.model.marketapi.OrderCreated;

/**
 * @author Kiril.m
 */
class BuyOrderTask implements Runnable {

	private static final Logger log = LogManager.getLogger(BuyOrderTask.class);
	private String apiKey;
	private BuyOrder buyOrder;

	public BuyOrderTask(String apiKey, BuyOrder buyOrder) {
		this.apiKey = apiKey;
		this.buyOrder = buyOrder;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		log.debug("Starting buy order task for " + buyOrder);
		if (BuyOrderManager.lockOrder(buyOrder.getId())) {
			OrderCreated orderCreated = BuyOrderManager.placeBuyLimit(	apiKey, buyOrder.getMarketName(),
																		buyOrder.getAmount().subtract(buyOrder.getFee()));
			if (orderCreated != null) {
				BuyOrderManager.unlockOrder(buyOrder.getId(), orderCreated.getUuid()); // update ... state = 3 where state = 2
			}
		} else {
			log.debug("The order could not be locked. Probably it's already locked");
		}
		log.debug("Task completed for buy order with id " + buyOrder.getId());
	}
}