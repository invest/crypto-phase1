package com.invest.crypto.services.order.buy;

import java.math.BigDecimal;

/**
 * @author Kiril.m
 */
class BuyOrder {

	private long id;
	private String marketName;
	private BigDecimal amount;
//	private BigDecimal quantity;
//	private BigDecimal rate;
	private BigDecimal fee;

	BuyOrder(long id, String marketName, BigDecimal amount, BigDecimal fee) {
		this.id = id;
		this.marketName = marketName;
		this.amount = amount;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMarketName() {
		return marketName;
	}

	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

//	public BigDecimal getQuantity() {
//		return quantity;
//	}
//
//	public void setQuantity(BigDecimal quantity) {
//		this.quantity = quantity;
//	}
//
//	public BigDecimal getRate() {
//		return rate;
//	}
//
//	public void setRate(BigDecimal rate) {
//		this.rate = rate;
//	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	@Override
	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getName() + ls
				+ super.toString() + ls
				+ "id: " + id + ls
				+ "marketName: " + marketName + ls
				+ "amount: " + amount + ls
				+ "fee: " + fee + ls;
//				+ "quantity: " + quantity + ls
//				+ "rate: " + rate + ls;
	}
}