/**
 * 
 */
package com.invest.crypto.bittrex.api;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.invest.crypto.bittrex.model.accountapi.Order;
import com.invest.crypto.bittrex.model.accountapi.OrderHistoryEntry;
import com.invest.crypto.bittrex.model.accountapi.WithdrawalRequested;
import com.invest.crypto.bittrex.util.Util;
import com.invest.crypto.security.SignatureUtil;

/**
 * @author Tabakov
 *
 */
//@Path("/accountapi")
public class BittrexAccountAPI {
	
	private static final Logger log = LogManager.getLogger(BittrexAccountAPI.class);
	private static final String BASE_URL = "https://bittrex.com/api/v1.1/account";

//	@GET
//	@Path("/getorder")//fully tested
//	@Produces(MediaType.APPLICATION_JSON)
	public Order getOrder(String uuid) throws JsonProcessingException, IOException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException {
		
		log.debug("getorder is called with params --> uuid: " + uuid);
		
		String nonce = SignatureUtil.createNonce();
		
		String url = BASE_URL + "/getorder?uuid=" + uuid + "&apikey=" + Util.API_KEY + "&nonce=" + nonce;
		
		String requestURL = "getOrderHistory URL: " + BASE_URL + "/getorder?uuid=" + uuid + "&nonce=" + nonce + "&API_KEY";
		
		log.debug(requestURL);
		
		String sign = SignatureUtil.calculateSignature(url);
		
		String jsonResponse = Util.getResponse(url, sign);
		
		Order order = new Order();
		order.setResponse(jsonResponse);
		order.setRequestURL(requestURL);
		
		
		log.debug("Bittrex response for getorderhistory: " + jsonResponse);

		ObjectMapper objectMapper = new ObjectMapper();

		JsonNode orderResponse = objectMapper.readTree(jsonResponse);
		String success = orderResponse.get("success").asText();
		if (success.equals("true")) {
			JsonNode result = orderResponse.get("result");
			if(!result.isNull()) {
				order.setAccountId(result.get("AccountId").isNull() ? null : result.get("AccountId").asText());
				order.setOrderUuid(result.get("OrderUuid").isNull() ? null : result.get("OrderUuid").asText());
				order.setExchange(result.get("Exchange").isNull() ? null : result.get("Exchange").asText());
				order.setType(result.get("Type").isNull() ? null : result.get("Type").asText());
				order.setQuantity(result.get("Quantity").isNull() ? null : result.get("Quantity").decimalValue());
				order.setQuantityRemaining(result.get("QuantityRemaining").isNull() ? null : result.get("QuantityRemaining").decimalValue());
				order.setLimit(result.get("Limit").isNull() ? null : result.get("Limit").decimalValue());
				order.setReserved(result.get("Reserved").isNull() ? null : result.get("Reserved").decimalValue());
				order.setReserveRemaining(result.get("ReserveRemaining").isNull() ? null : result.get("ReserveRemaining").decimalValue());
				order.setCommissionReserved(result.get("CommissionReserved").isNull() ? null : result.get("CommissionReserved").decimalValue());
				order.setCommissionReserveRemaining(result.get("CommissionReserveRemaining").isNull() ? null : result.get("CommissionReserveRemaining").decimalValue());
				order.setCommissionPaid(result.get("CommissionPaid").isNull() ? null : result.get("CommissionPaid").decimalValue());
				order.setPrice(result.get("Price").isNull() ? null : result.get("Price").decimalValue());
				order.setPricePerUnit(result.get("PricePerUnit").isNull() ? null : result.get("PricePerUnit").decimalValue());
				order.setOpened(result.get("Opened").isNull() ? null : LocalDateTime.parse(result.get("Opened").asText()));
				order.setClosed(result.get("Closed").isNull() ? null : LocalDateTime.parse(result.get("Closed").asText()));
				order.setIsOpen(result.get("IsOpen").isNull() ? null : result.get("IsOpen").asBoolean());
				order.setSentinel(result.get("Sentinel").isNull() ? null : result.get("Sentinel").asText());
				order.setCancelInitiated(result.get("CancelInitiated").isNull() ? null : result.get("CancelInitiated").asBoolean());
				order.setImmediateOrCancel(result.get("ImmediateOrCancel").isNull() ? null : result.get("ImmediateOrCancel").asBoolean());
				order.setIsConditional(result.get("IsConditional").isNull() ? null : result.get("IsConditional").asBoolean());
				order.setCondition(result.get("Condition").isNull() ? null : result.get("Condition").asText());
				order.setConditionTarget(result.get("ConditionTarget").isNull() ? null : result.get("ConditionTarget").asText());
			}
		}
		return order;
	}
	
//	@GET
//	@Path("/getorderhistory")// fully tested
//	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<OrderHistoryEntry> getOrderHistory(String market) throws JsonProcessingException, IOException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException {
		
		log.debug("getorderhistory is called with params --> market: " + market);
		
		String nonce = SignatureUtil.createNonce();
		
		String url = BASE_URL + "/getorderhistory?market=" + market + "&apikey=" + Util.API_KEY + "&nonce=" + nonce;
		
		String requestURL = "getOrderHistory URL: " + BASE_URL + "/getorderhistory?market=" + market + "&nonce=" + nonce + "&API_KEY";
		
		log.debug(requestURL);
		
		String sign = SignatureUtil.calculateSignature(url);
		
		String jsonResponse = Util.getResponse(url, sign);
		
		OrderHistoryEntry orderHistoryEntryResponse = new OrderHistoryEntry();
		
		orderHistoryEntryResponse.setResponse(jsonResponse);
		orderHistoryEntryResponse.setRequestURL(requestURL);
		
		
		log.debug("Bittrex response for getorderhistory: " + jsonResponse);

		ObjectMapper objectMapper = new ObjectMapper();
		ArrayList<OrderHistoryEntry> orderHistoryList = new ArrayList<OrderHistoryEntry>();
		// add whole Bittrex response to list
		orderHistoryList.add(orderHistoryEntryResponse);

		JsonNode orderResponse = objectMapper.readTree(jsonResponse);
		String success = orderResponse.get("success").asText();
		if (success.equals("true")) {
			JsonNode result = orderResponse.get("result");
			if(!result.isNull()) {
				if (result.isArray()) {
					for (JsonNode o : result) {
						OrderHistoryEntry orderHistoryEntry = new OrderHistoryEntry();
						orderHistoryEntry.setOrderUuid(o.get("OrderUuid").isNull() ? null : o.get("OrderUuid").asText());
						orderHistoryEntry.setExchange(o.get("Exchange").isNull() ? null : o.get("Exchange").asText());
						orderHistoryEntry.setTimeStamp(o.get("TimeStamp").isNull() ? null : LocalDateTime.parse(o.get("TimeStamp").asText()));
						orderHistoryEntry.setOrderType(o.get("OrderType").isNull() ? null : o.get("OrderType").asText());
						orderHistoryEntry.setLimit(o.get("Limit").isNull() ? null : result.get("Limit").decimalValue());
						orderHistoryEntry.setQuantity(o.get("Quantity").isNull() ? null :result.get("Quantity").decimalValue());
						orderHistoryEntry.setQuantityRemaining(o.get("QuantityRemaining").isNull() ? null :result.get("QuantityRemaining").decimalValue());
						orderHistoryEntry.setCommission(o.get("Commission").isNull() ? null :result.get("Commission").decimalValue());
						orderHistoryEntry.setPrice(o.get("Price").isNull() ? null :result.get("Price").decimalValue());
						orderHistoryEntry.setPricePerUnit(o.get("PricePerUnit").isNull() ? null :result.get("PricePerUnit").decimalValue());
						orderHistoryEntry.setIsConditional(o.get("IsConditional").isNull() ? null :result.get("IsConditional").asBoolean());
						orderHistoryEntry.setCondition(o.get("Condition").isNull() ? null :o.get("Condition").asText());
						orderHistoryEntry.setConditionTarget(o.get("ConditionTarget").isNull() ? null :o.get("ConditionTarget").asText());
						orderHistoryEntry.setImmediateOrCancel(o.get("ImmediateOrCancel").isNull() ? null :result.get("ImmediateOrCancel").asBoolean());
						orderHistoryList.add(orderHistoryEntry);
					}
				}
		  }

		}
		return orderHistoryList;
	}
	
	
	//@GET
	//@Path("/withdraw")// fully tested
	//@Produces(MediaType.APPLICATION_JSON)
	public WithdrawalRequested withdraw(String currency, BigDecimal quantity, String address) throws JsonProcessingException, IOException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException {
		
		log.debug("withdraw is called with params --> currency: " + currency + "quantity: " + quantity + "address: " + address);
		
		String nonce = SignatureUtil.createNonce();
		
		String url = BASE_URL + "/withdraw?currency=" + currency + "&apikey=" + Util.API_KEY + "&quantity=" + quantity + "&address=" + address + "&nonce=" + nonce;
		
		String requestURL = "withdraw URL: " + BASE_URL + "/withdraw?currency=" + currency + "&quantity=" + quantity + "&address=" + address + "&nonce=" + nonce + "&API_KEY";
		
		log.debug(requestURL);
		
		String sign = SignatureUtil.calculateSignature(url);
		
		String jsonResponse = Util.getResponse(url, sign);
		
		WithdrawalRequested withdrawalRequested = new WithdrawalRequested();
		
		withdrawalRequested.setResponse(jsonResponse);
		withdrawalRequested.setRequestURL(requestURL);
		
		log.debug("Bittrex response for withdraw: " + jsonResponse);

		ObjectMapper objectMapper = new ObjectMapper();

		JsonNode marketsResponse = objectMapper.readTree(jsonResponse);
		String success = marketsResponse.get("success").asText();
		if (success.equals("true")) {
			JsonNode result = marketsResponse.get("result");
			if(!result.isNull()) {
				withdrawalRequested.setUuid(result.get("uuid").isNull() ? null : result.get("uuid").asText());
			}
		}
		
		return withdrawalRequested;
	}
}
