package com.invest.crypto.bittrex.config;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.util.ManagerBase;

public class TickerManager extends ManagerBase {

	private static final Logger log = LogManager.getLogger(TickerManager.class);
	public static final long TIME_TO_WAIT_30_SEC = 30 * 1000;

	public static long getTickerHashInterval() {
		Connection con = null;
		try {
			con = getConnection();
			return TickerDAO.getTickerHashInterval(con);
		} catch (SQLException e) {
			log.debug("Unable to get ticker hash interval from DB, so we set 30 sec interval by default");
			log.debug("Unable to get ticker hash interval", e);
			return TIME_TO_WAIT_30_SEC;
		} finally {
			closeConnection(con);
		}
	}
}
