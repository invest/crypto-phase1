package com.invest.crypto.bittrex.config;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.invest.crypto.util.DAOBase;

public class TickerDAO extends DAOBase {
	private static final String TICKER_HASH_INTERVAL = "TICKER_HASH_INTERVAL";

	public static long getTickerHashInterval(Connection con) throws SQLException {
		String sql = "SELECT a.config_value FROM crypto_config a WHERE a.config_key = " + TICKER_HASH_INTERVAL;
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("config_value");
			}
		}
		return TickerManager.TIME_TO_WAIT_30_SEC;
	}

}
