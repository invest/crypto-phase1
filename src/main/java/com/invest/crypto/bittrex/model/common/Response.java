/**
 * 
 */
package com.invest.crypto.bittrex.model.common;

/**
 * @author Tabakov
 *
 */
public class Response {
	
	private String response;
	private String requestURL;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getRequestURL() {
		return requestURL;
	}

	public void setRequestURL(String requestURL) {
		this.requestURL = requestURL;
	}
	
	@Override
	public String toString() {
		return "Response [response=" + response + ", requestURL=" + requestURL + "]";
	}

}
