/**
 * 
 */
package com.invest.crypto.bittrex.util;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

/**
 * @author Tabakov
 *
 */
public class Util {
	
	public static final String HEADER_KEY = "apisign";
	// TODO these should be removed
	public static final String API_KEY = "c8406539054f48fbad6908940f4557e2";
	public static final String API_KEY_WITHDRAW = "c8406539054f48fbad6908940f4557e2";
	
	public static String getResponse(String url, String value) {
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);

		WebTarget target = client.target(UriBuilder.fromUri(url).build());
		String response = target.request(MediaType.APPLICATION_JSON).header(HEADER_KEY, value).get(String.class);
		return response;
	}
	
	public static String getResponse(String url) {
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);

		WebTarget target = client.target(UriBuilder.fromUri(url).build());
		String response = target.request(MediaType.APPLICATION_JSON).get(String.class);
		return response;
	}
	
	

	public static boolean isNull(String value) {
		if (value.equals("null")) {
			return true;
		} else {
			return false;
		}
	}

}
